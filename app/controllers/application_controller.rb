# @author jusef belkasemi
# This is the main controller of the application
class ApplicationController < ActionController::Base
  before_filter :authenticate_user!
  #protect_from_forgery  #causes a problem with json requests need to find a way arround this

  #this action is responsible for rendering the mai_menu paritial view
  def main_menu
    render :partial => 'menus/main_menu'
  end
end
